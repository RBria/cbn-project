package com.cbn.cbn_bria.service;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import java.time.Instant;
import java.util.Optional;

import com.cbn.cbn_bria.model.Computer;
import com.cbn.cbn_bria.repository.ComputerRepository;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service("computerService")
public class ComputerService {

	@Autowired
	ComputerRepository computerDao;

	@Transactional
	public Page<Computer> getAllComputers(Pageable pageable) {
		return computerDao.findAll(pageable);
	}

	@Transactional
	public Optional<Computer> getComputer(Long id) {
		return computerDao.findById(id);
	}

	@Transactional
	public Computer save(Computer computer) {
		if (computer.getId() == null && assertThatFirstInstanceIsLowerThanSecondInstance(computer.getIntroduced(),
				computer.getDiscontinued())) {
			return computerDao.save(computer);
		} else {
			throw new IllegalArgumentException();
		}
	}

	@Transactional
	public void deleteById(long id) throws IllegalArgumentException {
		computerDao.deleteById(id);
	}

	public Computer update(Long id, Computer computer) {
		computer.setId(id);
		if (computerDao.findById(computer.getId()).isPresent() && assertThatFirstInstanceIsLowerThanSecondInstance(
				computer.getIntroduced(), computer.getDiscontinued())) {
			return computerDao.save(computer);
		} else {
			throw new IllegalArgumentException();
		}
	}

	private Boolean assertThatFirstInstanceIsLowerThanSecondInstance(Instant instance1, Instant instance2) {
		// Compare the two Instants to check that the first one is lower than the second
		// one
		if (instance1 != null && instance2 != null) {
			if (!(instance1.compareTo(instance2) > 0)) {
				return true;
			}
			return false;
		}
		return true;
	}
}
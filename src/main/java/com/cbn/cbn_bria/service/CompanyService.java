package com.cbn.cbn_bria.service;

import java.util.Optional;

import com.cbn.cbn_bria.model.Company;
import com.cbn.cbn_bria.repository.CompanyRepository;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Page;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service("companyService")
public class CompanyService {

	@Autowired
	CompanyRepository companyDao;

	@Transactional
	public Page<Company> getAllCompanies(Pageable pageable) {
		return companyDao.findAll(pageable);
	}

	@Transactional
	public Optional<Company> getCompany(Long id) {
		return companyDao.findById(id);
	}

	@Transactional
	public Company save(Company company) {
		if (company.getId() == null) {
			return companyDao.save(company);
		} else {
			throw new IllegalArgumentException();
		}
	}

	@Transactional
	public void deleteById(long id) throws IllegalArgumentException {
		companyDao.deleteById(id);
	}

	public Company update(Long id, Company company) {
		company.setId(id);
		if (companyDao.findById(company.getId()).isPresent()) {
			return companyDao.save(company);
		} else {
			throw new IllegalArgumentException();
		}
	}
}
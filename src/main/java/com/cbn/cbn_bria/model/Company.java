package com.cbn.cbn_bria.model;

import java.util.Set;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;
 
/*
 * This is our model class and it corresponds to Country table in database
 */
@Entity
@Table(name="Company")
public class Company{
 
	@Id
	@Column(name="id")
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private Long id;
 
	@Column(name="name")
	private String name; 

	@OneToMany 
    private Set<Computer> computer;
 
	public Company() {
		super();
	}
	public Company(Long i, String name) {
		super();
		this.id = i;
		this.name = name;
	}
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public String getname() {
		return name;
	}
	public void setname(String name) {
		this.name = name;
	}
 
}
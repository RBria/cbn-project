package com.cbn.cbn_bria.model;

import java.time.Instant;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

/*
 * This is our model class and it corresponds to Country table in database
 */
@Entity
@Table(name = "Computer")
public class Computer {

	@Id
	@Column(name = "id")
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;

	@Column(name = "name")
	private String name;

	@Column(name = "introduced")
	private Instant introduced;

	@Column(name = "discontinued")
	private Instant discontinued;

	@ManyToOne
    @JoinColumn(name = "company_id", insertable = true, updatable = true)
	private Company company;

	public Computer() {
		super();
	}

	public Computer(Long i, String countryName, Instant introduced, Instant discontinued, Company company ) {
		super();
		this.id = i;
		this.name = countryName;
		this.introduced = introduced;
		this.discontinued = discontinued;
		this.company = company;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getCountryName() {
		return name;
	}

	public void setCountryName(String countryName) {
		this.name = countryName;
	}

	public Instant getIntroduced() {
		return introduced;
	}

	public void setIntroduced(Instant introduced) {
		this.introduced = introduced;
	}

	public Instant getDiscontinued() {
		return discontinued;
	}

	public void setDiscontinued(Instant discontinued) {
		this.discontinued = discontinued;
	}


	public Company getcompany() {
		return company;
	}

	public void setcompany(Company company) {
		this.company = company;
	}


}
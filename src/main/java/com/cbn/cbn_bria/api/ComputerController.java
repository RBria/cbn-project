package com.cbn.cbn_bria.api;

import com.cbn.cbn_bria.model.Computer;
import com.cbn.cbn_bria.service.ComputerService;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.data.domain.Pageable;

/**
 * ComputerController allow those methods :
 * getComputers,
 * getComputer,
 * createComputer,
 * updateComputer,
 * deleteComputer
 */
@RestController
public class ComputerController {

	@Autowired
	ComputerService computerService;

	/**
	 * getComputers fetch all computers based on the parameters
	 * @param pageable parameters that defined how to search companies
	 * @return a page of computer 
	 */
	@GetMapping(value = "/computers")
	public Page<Computer> getComputers(Pageable pageable) {

		Page<Computer> listOfComputers = computerService.getAllComputers(pageable);
		return listOfComputers;
	}

	/**
	 * getComputer fetch a computer based on the parameters
	 * @param id the id of the computer to fetch
	 * @return a computer
	 */
	@GetMapping(path = { "/computer/{id}" })
	public ResponseEntity<Computer> getComputer(@PathVariable long id) {
		return computerService.getComputer(id).map(record -> ResponseEntity.ok().body(record))
				.orElse(ResponseEntity.notFound().build());
	}

	/**
	 * createComputer create a computer based on the parameters
	 * @param computer the computer to create
	 * @return ResponseEntity that indicate if the computer has been well created
	 */
	@PostMapping(path = { "/computer" })
	public ResponseEntity<Computer> createComputer(@RequestBody Computer computer) {
		try {
			return ResponseEntity.ok().body(computerService.save(computer));
		} catch (IllegalArgumentException e) {
			return ResponseEntity.badRequest().build();
		}
	}

	/**
	 * updateComputer a computer based on the parameters
	 * @param id the computer to update
	 * @param computer the new computer fields
	 * @return ResponseEntity that indicate if the computer has been well updated
	 */
	@PutMapping(value = "/computer/{id}")
	public ResponseEntity<Computer> updateComputer(@PathVariable("id") long id, @RequestBody Computer computer) {
		try {
			return ResponseEntity.ok().body(computerService.update(id, computer));
		} catch (IllegalArgumentException e) {
			return ResponseEntity.badRequest().build();
		}
	}

	/**
	 * deleteComputer a computer based on the parameters
	 * @param id the computer to delete
	 * @return ResponseEntity that indicate if the computer has been well deleted
	 */
	@DeleteMapping(path = { "/computer/{id}" })
	public ResponseEntity<?> deleteComputer(@PathVariable long id) {
		try {
			computerService.deleteById(id);
			return ResponseEntity.ok().build();
		} catch (IllegalArgumentException e) {
			return ResponseEntity.notFound().build();
		}
	}
}
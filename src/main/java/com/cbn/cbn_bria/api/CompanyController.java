package com.cbn.cbn_bria.api;

import com.cbn.cbn_bria.model.Company;
import com.cbn.cbn_bria.service.CompanyService;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.data.domain.Pageable;

/**
 * CompanyController allow those methods :
 * getCompanies,
 * getCompany,
 * createCompany,
 * updateCompany,
 * deleteCompany
 */
@RestController
public class CompanyController {

	@Autowired
	CompanyService companyService;

	/**
	 * getCompanies fetch all companies based on the parameters
	 * @param pageable parameters that defined how to search companies
	 * @return a page of company 
	 */
	@GetMapping(value = "/companies")
	public Page<Company> getCompanies(Pageable pageable) {

		Page<Company> companies = companyService.getAllCompanies(pageable);
		return companies;
	}

	/**
	 * getCompany fetch a company based on the parameters
	 * @param id the id of the company to fetch
	 * @return a company
	 */
	@GetMapping(path = { "/company/{id}" })
	public ResponseEntity<Company> getCompany(@PathVariable long id) {
		return companyService.getCompany(id).map(record -> ResponseEntity.ok().body(record))
				.orElse(ResponseEntity.notFound().build());
	}

	/**
	 * createCompany create a company based on the parameters
	 * @param company the company to create
	 * @return ResponseEntity that indicates if the company has been well created
	 */
	@PostMapping(path = { "/company" })
	public ResponseEntity<Company> createCompany(@RequestBody Company company) {
		try {
			return ResponseEntity.ok().body(companyService.save(company));
		} catch (IllegalArgumentException e) {
			return ResponseEntity.badRequest().build();
		}
	}

	/**
	 * updateCompany a company based on the parameters
	 * @param id the company to update
	 * @param company the new company fields
	 * @return ResponseEntity that indicates if the company has been well updated
	 */
	@PutMapping(value = "/company/{id}")
	public ResponseEntity<Company> updateCompany(@PathVariable("id") long id, @RequestBody Company company) {
		try {
			return ResponseEntity.ok().body(companyService.update(id, company));
		} catch (IllegalArgumentException e) {
			return ResponseEntity.badRequest().build();
		}
	}

	/**
	 * deleteCompany a company based on the parameters
	 * @param id the company to delete
	 * @return ResponseEntity that indicates if the company has been well deleted
	 */
	@DeleteMapping(path = { "/company/{id}" })
	public ResponseEntity<?> deleteCompany(@PathVariable long id) {
		try {
			companyService.deleteById(id);
			return ResponseEntity.ok().build();
		} catch (IllegalArgumentException e) {
			return ResponseEntity.notFound().build();
		}
	}
}
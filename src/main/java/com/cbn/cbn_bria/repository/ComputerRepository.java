package com.cbn.cbn_bria.repository;

import com.cbn.cbn_bria.model.Computer;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
 
@Repository
public interface ComputerRepository 
    extends JpaRepository<Computer, Long> { }
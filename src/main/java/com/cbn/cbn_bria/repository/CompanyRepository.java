package com.cbn.cbn_bria.repository;

import com.cbn.cbn_bria.model.Company;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
 
@Repository
public interface CompanyRepository 
    extends JpaRepository<Company, Long> { }